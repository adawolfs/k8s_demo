from django.core.mail import send_mail, BadHeaderError
from django.template.loader import render_to_string

def sendEmail(user, url):
	msg_plain = render_to_string('email.html', {'password': user.password, 'url': url})
	msg_html = render_to_string('email.html', {'password': user.password, 'url': url})

	try:
		send_mail(
			'Kubernetes Demo',
			msg_plain,
			'kubernetes@demo',
			[user.email],
			html_message=msg_html,)
	except BadHeaderError:
		return HttpResponse('Invalid header found.')
		return HttpResponseRedirect('/contact/thanks/')