import sh
import time
import re
from django.template.loader import render_to_string


output_path = '/opt/k8s_demo/'

pods = ['liferay', 'nginx']
types = ['dep', 'serv', 'conf']
namespace = 'demo'


def gerUrl(uid):
	attempts = 1
	global external_ip
	while(True):
		external_ip = check_service('nginx-service-{}'.format(uid))
		print("External-IP " + external_ip + "\nAttempts: " + str(attempts))
		if(external_ip == "<pending>" and attempts < 600):
			time.sleep(5)
			attempts += 1
		else:
			break
	return external_ip


def check_service(service_name):
	output = sh.kubectl("--namespace", namespace, "get", "service", service_name)
	tuple = str(output).split('\n')
	data = re.split('\s+', tuple[1])
	external_ip = data[2]
	return external_ip



def generate(uid, email):
	mkdir(uid)
	makefiles(uid)
	fix_files(uid, email)
	smack_my_kube_up(uid)

def mkdir(uid):
	dir = output_path + str(uid)
	sh.mkdir(dir)

def makefiles(uid):
	dir = output_path + str(uid)
	for pod in pods:
		for type in types:
			sh.touch(dir + "/{}_{}_{}.yaml".format(pod, type, str(uid)))
	sh.touch(dir + "/nginx_conf_index" + str(uid) + ".yaml")


def fix_files(uid, email):
	gen_dep(uid)
	print("Deployment created")
	gen_serv(uid)
	print("Service created")
	gen_conf(uid, email)
	print("ConfigMap created")


def gen_dep(uid):
	dir = output_path + str(uid)
	for pod in pods:
		dep = open("{}/{}_dep_{}.yaml".format(dir, pod, str(uid)), 'w')
		dep.write(make_file('k8s/deploys/{}_dep.yaml'.format(pod), {'uid': str(uid)}))
	

def gen_serv(uid):
	dir = output_path + str(uid)
	for pod in pods:
		dep = open("{}/{}_serv_{}.yaml".format(dir, pod, str(uid)), 'w')
		dep.write(make_file('k8s/services/{}_serv.yaml'.format(pod), {'uid': str(uid)}))

def gen_conf(uid, email):
	dir = output_path + str(uid)

	dep = open("{}/{}_conf_{}.yaml".format(dir, 'nginx', str(uid)), 'w')
	dep.write(make_file('k8s/config/{}_conf.yaml'.format('nginx'), {'uid': str(uid)}))

	dep = open(dir + "/nginx_conf_index" + str(uid) + ".yaml", "w")
	content = render_to_string('k8s/config/nginx_index.yaml', {'uid': str(uid), 'email': email})
	dep.write(content)

def make_file(path, params):
	content = render_to_string(path, params)
	return content


def smack_my_kube_up(uid):
	print("Deploy files")
	dir = output_path + str(uid)
	sh.kubectl("--namespace", namespace, "create", "-f", dir)