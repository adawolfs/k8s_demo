from django import forms
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.base_user import BaseUserManager
from .models import User, Environment
from .emailUtils import sendEmail
from .gen_env import generate, gerUrl
from threading import Thread

# Create your views here.


class EmailForm(forms.Form):
	email = forms.EmailField()


def threaded_get_port(user):
	uid = str(user.id).zfill(3)
	url = gerUrl(uid)
	env = Environment(ip=url, port='80')
	env.save()
	user.environment = env
	user.save()
	sendEmail(user, url)

def create_thread(user):
	thread = Thread(target = threaded_get_port, args = (user, ))
	thread.start()

def index(request):
	if request.method == 'POST':
		form = EmailForm(request.POST)
		if form.is_valid():
			email_value = form.cleaned_data['email']
			user = User(email=email_value, password=BaseUserManager().make_random_password())
			user.save()
			uid = str(user.id).zfill(3)
			generate(uid, email_value)
			create_thread(user)
			return HttpResponseRedirect('/')
	else:
		form = EmailForm()

	return render(request, 'index.html', {'form': form})