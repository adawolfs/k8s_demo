from django.db import models

# Create your models here.

class Environment(models.Model):
	created = models.DateTimeField(auto_now_add=True)
	ip = models.CharField(max_length =  16)
	port = models.CharField(max_length =  5)
	
	def __str__(self):
		return self.ip + ":" + self.port


class User(models.Model):
	email = models.CharField(max_length=200)
	password = models.CharField(max_length=200)
	environment = models.ForeignKey(Environment, on_delete=models.CASCADE, null='True')

	def __str__(self):
		return self.email
